import os
import time
import shutil
import random
import cv_test
import convert_uni2ascii
import cv2
import pyvips
import sys
from random import shuffle

MAL_VECTOR = 'ംഃഅആഇഈഉഊഋഌഎഏഐഒഓഔകഖഗഘങചഛജഝഞടഠഡഢണതഥദധനഩപഫബഭമയരറലളഴവശഷസഹാിീുൂൃെേൈൊോൌ്ൎൗൺൻർൽൾ.,'

ASCII_VECTOR = '-+=!@#$%^&*(){}[]|\'"\\/?<>;:0123456789'

ENG_VECTOR = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz'

CHAR_VECTOR = list(MAL_VECTOR+ASCII_VECTOR)

global k

k =0

fonts = []
for file in os.listdir("data_generation_folder/"):
	if file.endswith(".TTF"):
		fonts.append(file)

unicode_fonts = ["AnjaliOldLipi","AnjaliOldLipi","Uroob","Karumbi","Chilanka","Dyuthi","Keraleeyam","Lohit Malayalam","Manjari","Meera", "RaghuMalayalam","Rachana","Rachana","Suruma","Suruma"]
unicode_size = list(range(150,300))
print(fonts)
sizes = [20,21,22,23,24,25,26,27,28,29,30,31,32,33,34,35,36,37,38,39,40,41,42,43,45,46,47,48,49,50,51,52,53,54,56,57,58,59,60,61,62,63,64,65,66,67,68,69,70]

rotates = [-3,-2,-1,0,1,2,3]

trans = ["gauss","poisson","speckle","blur",'blur']
trans1 = ["poisson","speckle","blur","blur"]
greys= ["#D0D0D0","#D8D8D8","#E0E0E0","#C0C0C0","#A9A9A9","#A0A0A0","#909090","#808080","#707070","#686868"]
font_colors = ["grey", "lightblue","blue","red","IndianRed","IndianRed4","crimson","DeepPink4","SpringGreen4","SpringGreen1","DarkGreen"]


def crop_to_sel(imge):
	img = cv2.cvtColor(imge, cv2.COLOR_BGR2GRAY)
	ret, thresh = cv2.threshold(img, 80, 255, cv2.THRESH_BINARY_INV)
	im2, contours, hierarchy = cv2.findContours(thresh, cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)
	X = []
	Y = []
	Xl = []
	Yl = []
	for cnt in contours:
		x,y,w,h = cv2.boundingRect(cnt)
		X.append(x)
		Y.append(y)
		Xl.append(x+w)
		Yl.append(y+h)
	try:	
		smallx = sorted(X)[0]
		largex = sorted(Xl)[-1]
		smally = sorted(Y)[0]
		largey = sorted(Yl)[-1]
		return imge[smally:largey,smallx:largex]
	except:
		return imge

def replace(word,char_to_be_removed):
	return word.replace(char_to_be_removed,'')

def create_new_data(file_name,dat):
	if permitted:
		f = open(file_name,'r')
		r = f.read()
		r = r.replace('\n',' ')
		randomized_arr = r.split(' ')
		shuffle(randomized_arr)
		for word in randomized_arr:
			if word != '':
				form = random.choice(['a','u','u'])
				if form == 'a':
					word_array = list(word)
					for w in word_array:
						if w not in list(MAL_VECTOR):
							word = replace(word,w)
					if word != '':
						font = random.choice(fonts)
						size = random.choice(sizes)
						rotate = random.choice(rotates)
						noise_typ = random.choice(trans)
						font_color = random.choice(font_colors)
						if size < 40:
							noise_typ ='a'
						grey = random.choice(greys)
						input_tex = word.replace("ല്‍",'ൽ').replace('ന്‍','ൻ').replace('ള്‍','ൾ').replace('ര്‍','ർ').replace('ണ്\u200d','ൺ')
						ascii_data = convert_uni2ascii.convert(input_tex,font.replace(".TTF",'').replace(".ttf",''))
						ascii_string = ascii_data[0].replace('\\','\\\\').replace('\u200c','').replace('\u200d','')
						if ascii_data[0] != "Error Occured. Please report this." and ascii_data[0] != '':
							os.system('convert -rotate %s -background \"%s\" -fill %s -font %s -size x%s -gravity center label:\'%s\' Data/%s.jpg' %(str(rotate),grey,font_color,font,str(size)," "+ascii_string+" ", str(k)))
							img = cv2.imread('Data/'+str(k)+'.jpg')
							img  = crop_to_sel(img)
							nois_img = cv_test.noisy(noise_typ,img)
							cv2.imwrite('Data/'+str(k)+'.jpg',nois_img)
							if k % 100 == 0:
								print(str(k)+" - "+ascii_string)
							dat.write(str(k)+"-"+input_tex)
							dat.write('\n')
							global k
							k = k+1
							if False:
								global permitted
								permitted = False
				else:
					word_array = list(word)
					for w in word_array:
						if w not in CHAR_VECTOR:
							word = replace(word,w)
					if word != '':
						font = random.choice(unicode_fonts)
						size = random.choice(unicode_size)
						noise_typ = random.choice(trans1)
						input_tex = word.replace("ല്‍",'ൽ').replace('ന്‍','ൻ').replace('ള്‍','ൾ').replace('ര്‍','ർ').replace('ണ്\u200d','ൺ')
						image = pyvips.Image.text(" "+input_tex+" ",dpi=size, font=font)
						image.write_to_file("x.png")
						imagem = cv2.imread("x.png")
						imagem = cv2.bitwise_not(imagem)
						imagem = crop_to_sel(imagem)
						nois_img = cv_test.noisy(noise_typ,imagem)
						cv2.imwrite('Data/'+str(k)+'.jpg',nois_img)
						if k % 100 == 0:
							print(str(k)+" - "+input_tex)
						dat.write(str(k)+"-"+input_tex)
						dat.write('\n')
						global k
						k = k+1
						if False:
							global permitted
							permitted = False						


global permitted
permitted = True
dat = open("Data/data.csv",'w')
for fil in os.listdir('data_generation_folder/pottan-ocr-data-master/traindata/'):
	if fil.endswith(".txt"):
		create_new_data('data_generation_folder/pottan-ocr-data-master/traindata/'+fil,dat)

dat.close()
print("completed")
