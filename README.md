# Malayalam OCR Data Generator

This is a module for preparing word level image data for Malayalam OCR. Though the font files make it limited only for Malayalam, you could bring in the necessary fonts of your choice language. Here I am using both Unicode and ASCII fonts for preparing the dataset.


## Libraries Used

<b>ImageMagick</b> (for obtaining images of ASCII fonts). 
<br> 
<b>PyVips</b> (for obtaining images of Unicode fonts).
<br>
<b>OpenCV</b> (for introducing noise and distortions in the generated images)

## Steps for Execution
 * Clone this repo.
 * Install the above said python packages.
 * Run `python image_gen.py`
 * Word data is obtained from Pottan-OCR-Repo , located in `data_generation_folder/pottan-ocr-data-master/traindata`, these text files are parsed, word-tokenized and sent into the processor.

## Acknowledgement
 * Data for words is taken from [Pottan-OCR](https://github.com/harish2704/pottan-ocr-data).

## Author:
 * Deepu Shaji, Research Assistant ICFOSS






