import cv2
import numpy as np
import random

def noisy(noise_typ,image):
	if noise_typ == "gauss":
		row,col,ch= image.shape
		mean = 0
		var = 0.1
		sigma = var**0.5
		gauss = np.random.normal(mean,sigma,(row,col,ch))
		gauss = gauss.reshape(row,col,ch)
		noisy = image + gauss
		return noisy
	elif noise_typ == "s&p":
		row,col,ch = image.shape
		s_vs_p = 1
		amount = 0.4
		out = np.copy(image)
		# Salt mode
		num_salt = np.ceil(amount * image.size * s_vs_p)
		coords = [np.random.randint(0, i - 1, int(num_salt)) for i in image.shape]
		out[coords] = 1
		num_pepper = np.ceil(amount* image.size * (1. - s_vs_p))
		coords = [np.random.randint(0, i - 1, int(num_pepper)) for i in image.shape]
		out[coords] = 0
		return out
	elif noise_typ == "poisson":
		vals = len(np.unique(image))
		vals = 2 ** np.ceil(np.log2(vals))
		noisy = np.random.poisson(image * vals) / float(vals)
		return noisy
	#elif noise_typ == "speckle":
	#	row,col,ch = image.shape
	#	gauss = np.random.randn(row,col,ch)
	#	gauss = gauss.reshape(row,col,ch)
	#	noisy = image + image * gauss
	#	return noisy
	elif noise_typ == "blur":
		hei = image.shape[0]
		blur = str(hei)[0]
		if blur == "1":
			blur = "2"
		noise_img = cv2.blur(image,(int(blur),int(blur)))
		return noise_img
	elif noise_typ == 'custom_uni.':
		g = range(75)
		h = range(150,255)
		total = range(0,175)
		gray_image = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
		for i in range(100):
			gray_image[np.where((gray_image==[random.choice(g)]))] = [random.choice(g)]
		for i in range(105):
			gray_image[np.where((gray_image==[random.choice(h)]))] = [random.choice(total)]
		gray_image[np.where((gray_image==[255]))] = [random.choice(range(50,255))]
		return gray_image
	else:
		return image


